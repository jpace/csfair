<?php
include ("top.php");
?>

<section id="main">
    <div class="row">
        <h1 class="page-title">About</h1>
    </div>
    <div class="row">
        <div class="col-md-4">

            <img src="images/about-1.JPG" alt="CS Fair" class="img-thumbnail img-about dis" />
            <img src="images/about-2.JPG" alt="CS Fair" class="img-thumbnail img-about dis" />
            <img src="images/about-3.JPG" alt="CS Fair" class="img-thumbnail img-about dis" />
            <img src="images/about-4.JPG" alt="CS Fair" class="img-thumbnail img-about dis" />
        </div>

        <div class="col-md-8">
            <div class="row">
                <div class="col-md-12">
                    <h2>What is the CS Fair?</h2>
                    <p>The CS Fair is a UVM event where Computer Science students (majors, minors or just taking a class) may submit and showcase their computer projects. The purpose of the CS Fair is to give students a chance to present their work to their peers and professionals in an open and exciting environment. A panel of judges critique the student work where the prize amounts are up to $300 with many opportunities to win. This year the CS Fair will take place in the Davis Center in the Grand Maple Ballroom.
                    </p>
                </div>

                <div class="col-md-12">
                    <h2>When</h2>
                    <p><?php echo FAIR_DATE . "<sup>th</sup> " . FAIR_YEAR . " " . FAIR_TIME; ?>

                        <br/>Davis Center, Grand Maple Ballroom, University of Vermont</p>
                    <iframe src="https://maps.google.com/maps?oe=utf-8&amp;client=firefox-a&amp;q=48+University+Place,+burlington,+vt+05405&amp;ie=UTF8&amp;hq=&amp;hnear=48+University+Pl,+Burlington,+Vermont+05405&amp;gl=us&amp;ll=44.479325,-73.199061&amp;spn=0.003957,0.005917&amp;t=h&amp;z=14&amp;output=embed"></iframe>
                </div>

                <div class="col-md-12">
                    <?php
                    include ("rules.php");
                    ?>
                </div>

                <div class="col-md-12">
                    <h2>Prizes &amp; Judging</h2>

                    <p>
                        Prizes are picked by a team of judges for each category. Student projects will be interspersed throughout the room but will be given a color code so a judge can easily identify which project to judge. The people's choice award goes to the projects with the most votes.
                    </p>
                    <h2>
                        <small>Random Hourly</small>
                    </h2>
                    <p>
                        Prizes are picked each hour via a random number generator. A winner will be selected for each hour, you must be present to claim your prize.
                    </p>
                </div>
            </div>
        </div>
    </div>  
</section> <!-- END MAIN CONTENT -->
<?php include ("footer.php"); ?>
</section> <!-- page-wrap in top.php-->
</body>
</html>

