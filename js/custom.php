<?php
/*
 * used on the sponsor form and registration form to show the extra form 
 * elements as needed
 */
?>
<script type="text/javascript">
<!--
    var xmlhttp;
    var groupSize = 2; // default group size for scheduling 1,2,3 get same processing
    
    // this function gets the file specified in the url
    function loadXMLDoc(url, group, category) {

        url = url + '?group_size=' + group + '&project_category=' + category;
        
        xmlhttp = null;
        if (window.XMLHttpRequest) {// code for Firefox, Opera, IE7, etc.
            xmlhttp = new XMLHttpRequest();
        } else if (window.ActiveXObject) {// code for IE6, IE5
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }

        if (xmlhttp != null) {
            xmlhttp.onreadystatechange = state_Change;
            xmlhttp.open("GET", url, true);
            xmlhttp.send(null);
        } else {
            //alert("Your browser does not support XMLHTTP.");
        }
    }


    //this function sets the radio buttons to hold the time and table
    function state_Change() {
        var timesAvail;
        
        if (xmlhttp.readyState == 4) {// 4 = "loaded"
           
            if (xmlhttp.status == 200) {// 200 = "OK"
                timesAvail = xmlhttp.responseText;
                
                var timeSlots = timesAvail.split(";");
                //"1,1:10 pm to 2:00 pm,21,north; 2,2:20 pm to 3:10 pm,23,south; 3,3:30 pm to 4:20 pm,14,north;";
                var timeOptions = "";
                var timeOptions = "<legend>Choose Preferred Time for Presenting</legend>"
                var tabIndex = 86;
                for (i = 0; i < timeSlots.length - 1; i++) {
                    slots = timeSlots[i].split(",");
                    timeOptions += '<label class="radio-inline">';
                    timeOptions += '<input type="radio" name="radTimeSlot" value="';
                    timeOptions += slots[0];
                    timeOptions += ', ';
                    timeOptions += slots[2];
                    timeOptions += ', ';
                    timeOptions += slots[3];
                    timeOptions += '" tabindex="';
                    timeOptions += (i + tabIndex);
                    timeOptions += '">';
                    timeOptions += slots[1];
                    timeOptions += '</label>';
                }
                document.getElementById("timeSlots").innerHTML = timeOptions;
            } else {
                document.getElementById("timeSlots").innerHTML = "<legend>Presentation Time Slots</legend><p>Contact rerickso@uvm.edu for presentation times</p><input type='hidden' value=' 1, 1, none' name='radTimeSlot'>";
            }
        }
    }

    function showNextGroup(index) {
        switch (index) {

            <?php
            //replaced const MAX_GROUP_SIZE with 12
            for ($i = 1; $i < 12; $i++) {
                print 'case ' . $i . ':';
                echo "\n";
                echo 'document.getElementById("group';
                echo $i + 1;
                echo '").style.display="block"; ';
                echo "\n";
                print 'document.getElementById("btnAdd' . $i . '").style.display="none"; ';
                echo "\n";
print 'var obj = document.getElementById("hidTimeSlot");';
print 'var timeSlotChosen = false;';
print 'if(typeof obj !== "undefined" && obj !== null) { ';
print ' timeSlotChosen = true;';
print '}';

print 'if(!timeSlotChosen) { ';        
                print "loadXMLDoc('../lib/is_timeslot_open.php', ";
                print $i + 2;
                print "," . "1" . ");";
print "}";                
echo "\n";
                print "groupSize = ";
		echo  $i + 2;  // used to calcualte a table
                echo "\n";
		print 'break; ';
                echo "\n";
            }
            ?>

        }
    }

    function showNextJudge(nextJudge) {
        switch (nextJudge) {

            <?php
            for ($z = 0; $z < MAX_JUDGES; $z++) {
                print "\n" . 'case ' . $z . ':' . "\n";
                print 'document.getElementById("judge';
                print $z + 1 . '").style.display="block";' . "\n";
                print 'document.getElementById("btnAdd' . $z . '").style.display="none";' . "\n";
                print 'document.getElementById("txtJudgesFirstName';
                print $z + 1 . '").focus();';
                print 'break;' . "\n";
            }
            ?>
        }
    }
//-->
</script>
