<div class="col-md-4 sidebar">
    <aside id="votingStatus">
        <div class="row intro-row"><h3>Current Voting Stats</h3><p>350 Total votes.</p><p>The real question is how many of these votes are for your project?</p></div> <!-- row intro-row -->
    </aside>
    <aside id="legend"><div class="row intro-row"><h2>Project Categories</h2><table id='categoryLegend'><tr ><th class="advanced-projects"> &nbsp;</th><td><a href='?cid=1'>Advanced Projects</a></td></tr><tr ><th class="intermediate-projects"> &nbsp;</th><td><a href='?cid=2'>Intermediate Projects</a></td></tr><tr ><th class="intermediate-web"> &nbsp;</th><td><a href='?cid=3'>Intermediate Web Design</a></td></tr><tr ><th class="beginner-projects"> &nbsp;</th><td><a href='?cid=4'>Beginner Programming</a></td></tr><tr ><th class="beginner-web"> &nbsp;</th><td><a href='?cid=5'>Beginner Web Design</a></td></tr><tr ><th class="research-projects"> &nbsp;</th><td><a href='?cid=6'>Research Projects</a></td></tr></table></div> <!-- row intro-row --></aside> <!-- legend -->          
</div> <!-- col-md-4 sidebar -->
