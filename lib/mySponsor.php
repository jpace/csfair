<?php

class mySponsor {

// print "<p>SQL: " . $sql . "<p><pre>"; print_r($data); print "</pre></p>";
// $this->db->select select($query, $values = "", $whereAllowed = true, $conditions = 0)
    var $db;

    public function __construct($db) {
        $this->db = $db;
    }

    //############################################################################
    function sponsorContact($sid) {
        if (empty($sid))
            return "";

        $sql = "SELECT fldContactEmail ";
        $sql .= "FROM tblSponsor ";
        $sql .= "WHERE pmkSponsorId = ?";


        $data = array((int) $sid);

        $results = $this->db->select($sql, $data, true, 0);

        return $results[0]["fldContactEmail"];
    }

//############################################################################
    function sponsorContactName($sid) {
        if (empty($sid))
            return "";

        $sql = "SELECT fldContactName ";
        $sql .= "FROM tblSponsor ";
        $sql .= "WHERE pmkSponsorId = ?";

        $data = array((int) $sid);
        $results = $this->db->select($sql, $data, true, 0);
        return $results[0]["fldContactName"];
    }

}

// end class
?>
