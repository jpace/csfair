<?php

class myJudge {

    // print "<p>SQL: " . $sql . "<p><pre>"; print_r($data); print "</pre></p>";
    // print "<p>Results:<pre>"; print_r($results); print "</pre></p>";
    // $this->db->select select($query, $values = "", $whereAllowed = true, $conditions = 0)
    var $db;

    public function __construct($db) {
        $this->db = $db;
    }

    //##########################################################################
    function judgeFound($judgeCode, $id) {
        if (empty($judgeCode))
            return "";
        if (empty($id))
            return "";

        $sql = "SELECT pkJudgeId ";
        $sql .= "FROM tblJudge ";
        $sql .= "WHERE fldJudgeCode = ? ";
        $sql .= "AND fkSponsorId = ? ";

        $data = array($judgeCode, $id);

        $results = $this->db->select($sql, $data, true, 1);
        if ($results) {
            if ($results[0]["pkJudgeId"] > 0) {
                return true;
            }
        }
        return false;
    }

    //##########################################################################
    function allJudges() {
        $sql = "SELECT DISTINCT pkJudgeId, fldFirstName, fldLastName, fldEmail, ";
        $sql .= "fldImageName, fldCompanyName, fldJobTitle, fldBio, fldTypeofJudge ";
        $sql .= "FROM tblJudge ";
        $sql .= "LEFT JOIN tblSponsor ON fkSponsorId=pmkSponsorId ";
        $sql .= "WHERE (tblSponsor.fldApproved = 1 "; // stops judge from automatically being shown
        $sql .= "OR tblJudge.fldApproved = 1) "; // allows judge to work for a company that is not sponsoring us.
        $sql .= "ORDER BY fldLastName, fldFirstName";

        $results = $this->db->select($sql, "", true, 2);

        return $results;
    }

}

?>
