<?php

class Category {

// $this->db->select select($query, $values = "", $whereAllowed = true, $conditions = 0)
    var $db;

    public function __construct($db) {
        $this->db = $db;
    }

    //############################################################################
    function categoryName($id) {
        $id = (int) $id;
        $sql = "SELECT fldCategory ";
        $sql .= "FROM tblCategory ";
        $sql .= "WHERE pmkCategoryId = ? ";

        $results = $this->db->select($sql, array($id), true, 0);

        return $results[0][0];
    }
    //############################################################################
    function approvedCategories() {

        $sql = "SELECT pmkCategoryId, fldCategory, fldMaxEntries ";
        $sql .= "FROM tblCategory ";
        $sql .= "WHERE fldApproved = 1 ";
        $sql .= "ORDER BY fldDisplayOrder";

        $results = $this->db->select($sql, "", true, 0);

        return $results;
    }

    function listboxOfCategories($tabindex = 0, $formList = 0, $default = "") {

        $categories = $this->approvedCategories();

        print '<fieldset  class="listbox">';
        print '<label class="required" for="lstCategory">Judge in Category </label>';
        print '<select id="lstCategory" ';

        if ($formList) {
            if ($projectID < 1) {
                print 'onChange="loadXMLDoc(';
                print "'../lib/is_timeslot_open.php'";
                print ", 1";
                print ', this.value);" ';
            }
        }

        print 'name="lstCategory" ';
        print 'tabindex="' . $tabindex . '" >';

        foreach ($categories as $category) {
            print '<option ';

            if ($default == $category["pmkCategoryId"])
                print " selected ";

            print 'value="' . $row["pmkCategoryId"] . '">' . $row["fldCategory"];
            print '</option>';
        }
        print '</select>';
        print '</fieldset>';
    }

}

// end class
?>
