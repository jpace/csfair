<?php

function addScheme($url, $scheme = 'http://') {
    return parse_url($url, PHP_URL_SCHEME) === null ?
            $scheme . $url : $url;
}

function verifyAlphaNum($testString) {
    // Check for letters, numbers and dash, period, space and single quote only. 
    return (preg_match("/^([[:alnum:]]|-|\.| |')+$/", $testString));
}

function verifyAlphaNumComma($testString) {
    // Check for letters, numbers and dash, period, space and single quote only. 
    return (preg_match("/^([[:alnum:]]|-|\.|,| |')+$/", $testString));
}

function verifyEmail($testString) {
    // Check for a valid email address 
    return filter_var($testString, FILTER_VALIDATE_EMAIL);
}

function verifyLink($testString) {
    return filter_var($url, FILTER_VALIDATE_URL);
}

function verifyText($testString) {
    // Check for letters, numbers and dash, period, ?, !, space and single and double quotes only. 
    return (preg_match("/^([[:alnum:]]|-|\.| |\n|\r|\?|\!|\"|\')+$/", $testString));
}

function verifyPhone($testString) {
    // Check for only numbers, dashes and spaces in the phone number 
    return (preg_match('/^([[:digit:]]| |-)+$/', $testString));
}

function verifyURL($testString) {
    // Check for a valid URL
    if (filter_var($testString, FILTER_VALIDATE_URL)) {
        $file_headers = @get_headers($testString);
        if ($file_headers[0] == 'HTTP/1.1 404 Not Found'
                OR $file_headers == "") {
            return false;
        } else {
            return true;
        }
    }
    return false;
}

function verifyHTTP($testString) {
    // Check for a valid URL
    $found = strpos(strtolower($testString), "http");
    if ($found === false) {
        return false;
    }
    return true;
}

function verifyLength($testString, $length) {
    if (strlen($testString) < $length) {
        return false;
    }
    return true;
}

//############################################################################
// if name is missing get it        
function ldapStudentName($uvmID) {
    if (empty($uvmID))
        return false;
    global $thisDatabase;
    $name = "na";
    $ds = ldap_connect("ldap.uvm.edu");
    if ($ds) {
        $r = ldap_bind($ds);
        $dn = "uid=$uvmID,ou=People,dc=uvm,dc=edu";
        $filter = "(|(netid=$uvmID))";
        $findthese = array("sn", "givenname");
        // now do the search and get the results which are storing in $info
        $sr = ldap_search($ds, $dn, $filter, $findthese);
        // if we found a match (in this example we should actually always find just one
        if (ldap_count_entries($ds, $sr) > 0) {
            $info = ldap_get_entries($ds, $sr);
            $name = $info[0]["givenname"][0] . " " . $info[0]["sn"][0];
            $query = "INSERT tblStudent set ";
            $query.="pkUsername = ?, ";
            $query.=" fldFirstName = ?, ";
            $query.=" fldLastName = ? ";
            $query.=" ON DUPLICATE KEY UPDATE fldFirstName = ?, ";
            $query.=" fldLastName= ?";
            if (DEBUG)
                print "<p>sql " . $query;
            $thisDatabase->select($query, array($uvmID, $info[0]["givenname"][0], $info[0]["sn"][0], $info[0]["givenname"][0], $info[0]["sn"][0]));
        }
    }
    ldap_close($ds);
    if ($name == "na") {
        return false;
    }
    return true;
}

function upload($htmlNameInput, $pmkImageId) {

    /*     * * check if a file was uploaded ** */
    if (is_uploaded_file($_FILES[$htmlNameInput]['tmp_name']) && getimagesize($_FILES[$htmlNameInput]['tmp_name']) != false) {
        global $thisDatabase;

        /*         * *  get the image info. ** */
        $size = getimagesize($_FILES[$htmlNameInput]['tmp_name']);
        /*         * * assign our variables ** */
        $type = $size['mime'];
        $imgfp = fopen($_FILES[$htmlNameInput]['tmp_name'], 'rb');
        $size = $size[3];
        $name = $_FILES[$htmlNameInput]['name'];


        $tableName = "";

        $data = array($pmkImageId, $type, $imgfp, $size, "S", $name);
        switch ($htmlNameInput) {
            case "sponsor";
                $tableName = "tblSponsorLogo";
                break;
        }

        $sql = 'INSERT INTO ' . $tableName;

        $sql .= '(fnkImageId, fldImageType, fldImage, fldImageSize, fldImageCtgy, fldImageName) ';
        $sql .= 'VALUES (? ,?, ?, ?)';
        $stmt = $thisDatabase->insert($sql, $data);
        return $stmt;
    }
}

?> 