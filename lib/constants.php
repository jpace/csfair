<?php
// print "<p>SQL: " . $sql . "<pre>"; print_r($data); print "</pre></p>";     
// print "<p>Array:<pre>"; print_r($results); print "</pre></p>";


// year specific variables
define("DATABASE_NAME", strtoupper(get_current_user()) . '_2017');

define("FAIR_YEAR", "2017");

define("FAIR_DATE", "December 9");

define("FAIR_TIME", "1:10 PM to 4:20 PM");

define("WEB_MASTER", 'rerickso@uvm.edu');


define("MAX_JUDGES", 5);
/* if you change this number you will need to update the 
*  sponsor form in two places
*  roughly line 255 find: " fifth ";
*  roughly line 725, find: print "Fifth "; 
*/

define("MAX_GROUP_SIZE", 12);

define("MAX_LOGO_SIZE", 90000000);

define("PRIZE_COLUMNS", 3);
$prizeColumns = 3;
//turn off or on whether i check if a media resource exists
// ie we look for url to see if not found
define ("URL_CHECKING", false);


// general setup constants for system 
// sanitize the server global variable
$_SERVER = filter_input_array(INPUT_SERVER, FILTER_SANITIZE_STRING);

// sanatize GET global variables
if (!empty($_GET)) {
    $_GET = filter_input_array(INPUT_GET, FILTER_SANITIZE_STRING);
}

define("CURRENT_TIME", time());

define("DEBUG", false);

define("DB_DEBUG", false);

define("LINE_BREAK", "\n");

define("TODAY", date('Y-m-d', mktime(0, 0, 0, date('m'), date('d'), date('Y'))));



define ("SERVER", htmlentities($_SERVER['SERVER_NAME'], ENT_QUOTES, "UTF-8"));

define("DOMAIN", "//" . SERVER);

define ("PHP_SELF", htmlentities($_SERVER['PHP_SELF'], ENT_QUOTES, "UTF-8"));

$PATH_PARTS = pathinfo(PHP_SELF);

if(strpos($PATH_PARTS['dirname'], 'dev')){
define ("BASE_PATH", '//rerickso.w3.uvm.edu/Blackboard-dev/cs142/samples/csfair/');
}else{
  define ("BASE_PATH", '//rerickso.w3.uvm.edu/Blackboard-live/cs142/samples/csfair/');  
}


// sometimes you want to know where www-root is located in relation to where you
// are. Just count the / and then create the path
$www_rootPath="";
for($i=1; $i<substr_count(PHP_SELF, '/'); $i++){
    $www_rootPath .= "../";
}

define("WEB_ROOT_PATH", $www_rootPath);

// generally I put my passwords outside of the www-root folder so it is not
// in a public folder at all. The web server can access it so still don't
// print your passwords with php code
define("BIN_PATH", $www_rootPath . "../bin");

// here my lib folder is just in the same folder but you may have set up your
// lib folder in the www-root so its common to all your projects. If that is the
// case you would just define it like the bin path without going up a level more:
// define("LIB_PATH", $www_rootPath . "lib");
define("LIB_PATH", "lib");

define("CSS_PATH", BASE_PATH . 'css');
define("JS_PATH", BASE_PATH . 'js');
/*
            print "<p>Domain: " . DOMAIN;
            print "<p>php Self: " . PHP_SELF;
            print "<p>Path Parts<pre>";
            print_r($PATH_PARTS);
            print "</pre></p>";
            print "<p>BASE_PATH: " . BASE_PATH;
            print "<p>WEB_ROOT_PATH: " . WEB_ROOT_PATH;
            print "<p>BIN_PATH: " . BIN_PATH;
            print "<p>LIB_PATH: " . LIB_PATH;
            print "<p>CSS_PATH: " . CSS_PATH;
            print "<p>JS_PATH: " . JS_PATH;
*/
?>