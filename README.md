<p>This repository is designed so you can study the underlying code so you can create the style sheet for the CS Fair. You should not change the underlying structure 
because if you do then your css wont work on the live site. On the forms i have listed one error and corresponding class mistake so you can style those elements as 
this is a non functioning website. Images however use generic names so just name your images to match the names used in the code.</p>

<p>You should notice there 

are two structures, index.php (index, judges, sponsors all on one page) and index-2.php (all separate pages). Create a style for both.</p>

<p>You can see the live 
site at <a target="_blank" href="http://csfair.w3.uvm.edu/">http://csfair.w3.uvm.edu/</a></p>

<p>Git commands<pre>
 Command line instructions
Git global setup

git config
 --global user.name "Robert Michael Erickson"
git config --global user.email "robert.erickson@uvm.edu"

Create a new repository

git clone git@gitlab.uvm.edu:robert-erickson/CS-Fair
-Template-for-CSS.git
cd CS-Fair-Template-for-CSS
touch README.md
git add README.md
git commit -m "add README"
git push -u origin
 master

Existing folder

cd existing_folder
git init
git remote add origin git@gitlab.uvm.edu:robert-erickson/CS-Fair-Template-for-CSS.git
git add .
git 
commit
git push -u origin master

Existing Git repository

cd existing_repo
git remote add origin git@gitlab.uvm.edu:robert-erickson/CS-Fair-Template-for-CSS.git
git
push -u origin --all
git push -u origin --tags
</pre></p>