<?php
include ("top.php");


                $category = 0;
                $sortOrder = "None";
                $sortDisplay = "None";

                if (isset($_GET["cid"])) {
                    $category = (int) $_GET["cid"];
                    $sortOrder = $thisProject->sortORder((int) $_GET["cid"]);
                    $sortDisplay = $thisProject->sortDisplay((int) $_GET["cid"]);
                }

                $rstProjects = $thisProject->allProjects($category, $sortOrder);

                $num_rows = 0;
                if (!empty($rstProjects)) {
                    $num_rows = count($rstProjects);
                }
?>

<section id="main">

    <div class="row">
        <h1 class="page-title">Projects List by Category: <?php
        if($category){
        print $thisCategory->categoryName($category);
        }else{
           print "Choose category";
        }
        ?></h1>
        <hr/>
    </div> <!-- row -->

    <div class="row">
        <div class="col-md-8">
            <p class="lead">
                <?php
                $display = true;

                if (!$display) {
                    print "Please check back after registration opens in October.</p></div>";
                } else {
                    ?>
                    <!--Registration is open but space is limited.-->
                </p>


            </div> <!-- col-md-8 -->

        </div><!-- row -->

        <div class="row">
            <div class="col-md-10">
                <?php
                /*           print "<p>Sort by: <a href='?g=50'>Category</a> | ";
                  print "<a href='?g=30'>Advanced Projects</a> | ";
                  print "<a href='?g=20'>Booth Number</a> | ";
                  print "<a href='?g=40'>Title</a> | ";
                  print "<a href='?g=1'>Random</a></p>";
                 */

                // wrong method :( print $thisCategory->listOfCategories($tabindex++, 1, $category);
                
                print $thisProject->categoryLegend("", $category);
                
                print "<p></p>";
                print "<p>A Yellow background with red text means a project has not been confirmed and a students needs to check their email and click on Confirm.</p>";
                print "<p>A Yellow background with black text means it has been confirmed but i have not approved it. Most likely I have just not had time</p>";
                print "<p>Total Projects: " . $num_rows .
                        "</p>";

if(is_array($rstProjects)) {
    foreach ($rstProjects as $oneProject) {

                    print '<table class="table table-striped table-bordered">';
                    print '<thead>';
                    print '<tr><th ';
                    print 'class="';
                    if ($oneProject['fldConfirmed'] == 0) {
                        print "notConfirmed ";
                    }

                    if ($oneProject['fldApproved'] == 0) {
                        print "notApproved ";
                    }
                    print '">';

                    if ($oneProject['fldProjectSite'] != "") {
                        print '<a href="' . $oneProject['fldProjectSite'] . '">';
                    }

//why get rid of the screen shot? nobody makes them :(
//
                    // if($oneProject['fldProjectImageURL']!=""){
                    //     print '<img src="' . $oneProject['fldProjectImageURL'] . '" class="gravatar" alt="" >';
                    // }

                    print $oneProject['fldProjectName'];

                    if ($oneProject['fldProjectSite'] != "") {
                        print '</a>';
                    }

                    print " - " . $oneProject['pmkProjectId'];
                    print '</th></tr>';
                    print '</thead>';

                    //add first name and last to student table set up ldap to retrieve names
                    print '<tr><td><strong>By:</strong> ';

                    $rstGroup = $thisProject->projectMembers((int) $oneProject['pmkProjectId']);
                    $lastElement = end($rstGroup);
if(is_array($rstGroup)) {
                    foreach ($rstGroup as $student) {
                        if ($student == $lastElement) {
                            print $student['fldFirstName'] . "&nbsp;" . $student['fldLastName'];
                        } else {
                            print $student['fldFirstName'] . "&nbsp;" . $student['fldLastName'] . ", ";
                        }
                    }
}
                    print '</td></tr>';

                    print '<tr><td><strong>Category:</strong> ';
                    print '<span style="background-color: ' . $oneProject['fldColorCode'] . '">' . $oneProject['fldCategory'] . '</span>';
                    print '</td></tr>';

                    /* liking not implemented 
                      print '<a href="vote?g=' .
                      $oneProject['pmkProjectId']
                      . '" class="voteButton"><span>6</span> Likes</a>';
                     */
                    print '<tr><td><strong>Booth:</strong> ';

                    if ($oneProject['fldProjectBoothNum'] > 0) {
                        print $oneProject['fldProjectBoothNum'] . " - " . $oneProject['fldBoothSide'];
                    } else {
                        print "Not Assigned Yet";
                    }

                    print '</td></tr>';

                    print '<tr><td><strong>Presentation Time:</strong> ';
                    if ($oneProject['fldTime'] != "") {
                        print $oneProject['fldTime'];
                    } else {
                        print "Not Assigned Yet";
                    }

                    print '</td></tr>';

                    print '<tr><td><strong>Related Course(s):</strong> ';
                    print $oneProject['fldProjectCourses'];
                    print '</td></tr>';

                    print '<tr><td><strong>Description:</strong> ';
                    print $oneProject['fldProjectDesc'];
                    print '</td></tr>';

                    print '</table>';
                }
}
                ?>
            </div> <!-- col-md-10 -->
            <?php
        } // ends display till september 
        ?>
    </div> <!-- row -->
</section> <!-- main -->


<?php
include ("footer.php");
?>
</section> <!-- page-wrap -->
</body>
</html>
