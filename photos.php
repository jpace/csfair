<?php
include ("top.php");
?>
<section id="main"> <!-- MAIN CONTENT -->
    <div class="row intro-row">
        <h1 class="page-title">Gallery</h1>

        <section id="photoGallery">
            <?php

            // this function gets all the jpg images locaged in the $url
            // It returns an array with the image names. folder must not have an index file
            function getFileList($url, $extensions = array("jpg", "png")) {
                $outputBuffer = array();

                $dir = scandir($url);

                if (count($dir) > 0) {
                    //Start at index 2, to ignore the ".." and "." folders
                    for ($i = 2; $i < count($dir); $i++) {
                        //Only add files to the image array that have the expected extension
                        $ext = pathinfo($dir[$i], PATHINFO_EXTENSION);
                        if (in_array($ext, $extensions)) {
                            array_push($outputBuffer, $dir[$i]);
                        }
                    }
                }

                return $outputBuffer;
            }

            $url = "./images/gallery/";

            $images = getFileList($url);

            if (is_array($images)) {
                foreach ($images as $img) {
                    print '<img src="' . $url . '/' . $img . '" alt="">' . "\n";
                }
            }
            ?>
        </section> <!-- photo gallery -->
    </div>
</section> <!-- main -->
</section> <!--page-wrap -->
<?php
include ("footer.php");
?>
</body>
</html>
