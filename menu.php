<nav class="navbar navbar-default" role="navigation">
    <div class="container-fluid">

        <!-- mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ol class="nav navbar-nav">
                <?php
                print '<li';
                if (basename(PHP_SELF) == "./index.php") {
                    print ' class="active"';
                }
                print '><a href="./index.php">Home</a></li>';

                print '<li';
                if (basename(PHP_SELF) == "./about.php") {
                    print ' class="active"';
                }
                print '><a href="./about.php">About</a></li>';

                print '<li';
                if (basename(PHP_SELF) == "./judges.php") {
                    print ' class="active"';
                }
                print '><a href="./index.php#judges">Judges</a></li>';

                print '<li';
                if (basename(PHP_SELF) == "./sponsors.php") {
                    print ' class="active"';
                }
                print '><a href="./index.php#sponsors">Sponsors</a></li>';

                print '<li';
                if (basename(PHP_SELF) == "./projects.php") {
                    print ' class="active"';
                }
                print '><a href="./projects.php">Projects</a></li>';

                print '<li';
                if (basename(PHP_SELF) == "./schedule.php") {
                    print ' class="active"';
                }
                print '><a href="./schedule.php">Schedule</a></li>';

                print '<li';
                if (basename(PHP_SELF) == "./photos.php") {
                    print ' class="active"';
                }
                print '><a href="./photos.php">Gallery</a></li>';
                ?>
            </ol>
        </div>
        <!-- /.navbar-collapse -->
    </div>
</nav>
