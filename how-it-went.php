<?php
include ("top.php");
?>

<section id="main">
  <div class="row">
        <h1 class="page-title">How it went!</h1>
    </div>
    <div class="row">
        
        <h2>The Layout</h2>
        <a href="./images/layout.png">Our Layout Picture</a>
        
        <h2>Summary</h2>
        <p>
            The project went well, we kept in communication throughout the project and were understanding of each other's time tables. The end result looks very neat,
            considering how the CSS changes that had to be made, and that we were essentially starting from the ground up. We think the website is fairly clean,
            well structured, and the colors work well together making for a usable website. 
        </p>
        
        <h2>Key CSS Features</h2>
        <ul>
            <li> The navigation bar is placed at the top of the website for both styles. There are hover effects and active effects that allow the user to know what page
            they are on and what page they are clicking. A combination of colors from the color scheme of each style highlights the Navigation Bar to make it easy to use.</li>
            <li>The site logo, date and time of the event, and caption are all placed between the main content and the Navigation Bar to provide the info and title on each page.
                These are consistent throughout the website, to give a uniform approach to the design. These are modified between both unique styles. </li>
            <li> There is a center div clearly marked with a thin black border and background color that makes the element and content stand out. This draws the user's attention to 
                the information they need. There are also underlined and emphasized headers to give user's an idea of the information available on the page. These are also colored with
            the theme of the page, which varies between each unique style.</li>
            <li>Having a layout where only a certain part of the website changes when the user navigates around stops the user from having to learn new layouts and search for things they
                have already found, which fosters trust with the user!</li>
        </ul>
        <h2>Our Explanation Video</h2>
        
        <a href="https://www.youtube.com/watch?v=iN-BAsZX-rs&feature=youtu.be">The Video</a>
       
    </p>
  </div>
</section>
